import 'jsdom-global/register';
import 'mocha';
import {expect} from 'chai';
import {configure} from 'enzyme';
import * as React from 'react';

import * as Adapter from 'enzyme-adapter-react-16';

import {IState, IProps, updatePath} from './index';

configure({adapter: new Adapter()});

describe('Hello Component', () => {
    it('sets correct path', () => {
        const state: IState = {
            path: ''
        };

        const props = {
            match: {
                params: {
                    path: 'react'
                }
            }
        };

        const newState = updatePath(state as IState, props as IProps);
        expect(newState).to.have.property('path');
        expect(newState.path).to.equal('react').and.be.a('string');
    });
});
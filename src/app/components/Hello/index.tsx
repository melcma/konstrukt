import * as React from 'react';
import {RouteComponentProps} from 'react-router-dom';

interface IProps extends RouteComponentProps<any>, React.Props<any> {
}

interface IState {
    path: string
}

function updatePath(state: IState, props: IProps) {
    return {path: props.match.params.path}
}

class Hello extends React.Component<IProps, IState> {
    constructor(props: IProps) {
        super(props);

        this.state = {
            path: this.props.match.params.path
        }
    }

    componentWillReceiveProps() {
        this.setState((state: IState, props: IProps) => updatePath(state, props));
    }

    render() {
        return (
            <h1>Hello {this.state.path}</h1>
        )
    }
}

export {IState, IProps, updatePath};
export default Hello;
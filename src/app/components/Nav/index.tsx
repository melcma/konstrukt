import * as React from 'react';
import {Link} from 'react-router-dom';

const Nav = () => {
    return (
        <nav style={{display: 'flex', flexDirection: 'column'}}>
            <Link to='/'>Home</Link>
            <Link to='/react'>React</Link>
            <Link to='/typescript'>Typescript</Link>
        </nav>
    )
};

export default Nav;
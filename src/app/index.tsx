import * as React from 'react';
import {Switch, Route} from 'react-router-dom';

import Hello from './components/Hello';
import Nav from './components/Nav';

const App = () => {
    return (
        <React.Fragment>
            <Nav/>
            <br/>
            <Switch>
                <Route exact path="/" component={Hello}/>
                <Route path="/:path" component={Hello}/>
            </Switch>
        </React.Fragment>
    );
};

export default App;
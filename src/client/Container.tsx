import * as React from 'react';
import {BrowserRouter as Router} from 'react-router-dom';

import App from '../app';

const Container = () => {
    return (
        <Router>
            <App/>
        </Router>
    );
};

export default Container;
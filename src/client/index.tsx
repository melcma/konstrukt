import * as React from 'react';
import * as ReactDOM from 'react-dom';

import Container from './Container';

import '../styles/main.scss';

if ((module as any).hot) {
    (module as any).hot.accept();
}

ReactDOM.render(<Container/>, document.querySelector('.js-app'));
import 'mocha';
import {expect} from 'chai';
import {shallow, configure} from 'enzyme';
import * as React from 'react';

import * as Adapter from 'enzyme-adapter-react-16';

import App from '../app/index';
import Container from './Container';

configure({adapter: new Adapter()});

describe('Frontend Container', () => {
    it('renders application', () => {
        const container: any = shallow(<Container />);
        expect(container.find(App)).to.have.length(1);
    });
});
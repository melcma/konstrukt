import * as fs from 'fs';
import * as path from 'path';

import * as express from 'express';
import * as React from 'react';
import {renderToNodeStream} from 'react-dom/server';
import {StaticRouter as Router} from 'react-router';

import App from './../app/index';

const server = express();
const env: string = process.env.NODE_ENV || 'development';
const port: string = process.env.PORT || '8003';
const html: string = fs.readFileSync('./dist/index.html').toString();
const htmlParts: Array<string> = html.split('...');
const distPath = path.join(__dirname, '../../dist');

server.use(express.static(distPath, {
    index: false
}));

server.use((req, res) => {
    const context: object = {};
    const markup = (
        <Router location={req.url} context={context}>
            <App/>
        </Router>
    );

    res.write(htmlParts[0]);

    const stream = renderToNodeStream(markup);
    stream.pipe(res, {end: false});
    stream.on('end', () => {
        res.write(htmlParts[1]);
        res.end();
    });
});

server.listen(port, () => console.log(`Server listening on port ${port} in ${env} mode`));

export default server;
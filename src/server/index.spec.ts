import * as request from 'supertest';
import {expect} from 'chai';
import 'mocha';

import server from './index';

describe('express rendering', () => {
    it ('server responds with 200',(done) => {
        request(server)
            .get('/')
            .end((err, res) => {
                if (err) throw err;
                expect(res.status).to.equal(200);
                done();
            })
    });
});

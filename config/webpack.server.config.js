const path = require('path');
const nodeExternals = require('webpack-node-externals');

module.exports = {
    target: 'node',
    externals: [nodeExternals()],
    entry: './src/server/index.tsx',
    output: {
        path: path.resolve(__dirname, '../dist'),
        filename: 'server.js'
    },
    module: {
        rules: [
            {
                test: /\.(ts|tsx)$/,
                exclude: /node_modules/,
                loader: 'awesome-typescript-loader'
            }
        ]
    },
    devServer: {
        contentBase: path.join(__dirname, './src'),
        watchContentBase: true,
        port: 9003,
        host: '0.0.0.0',
        historyApiFallback: true,
        watchOptions: { poll: true }
    },
    resolve: {
        extensions: ['.js', '.ts', '.tsx']
    }
};
# Konstrukt
React template with TypeScript and Server Side Rendering support. Simple and lean as possible.

Includes:
- React 16
- TypeScript
- SCSS
- Babel
- Express SSR
- Mocha, Chai, Enzyme
- Docker
- Ansible
- Gitlab Pipelines full deployment cycle
